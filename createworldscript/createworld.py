#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys
import logging
import uuid
import datetime
import configparser
try:
    import cpickle as pickle
except ModuleNotFoundError:
    import pickle

LOG_DIR = "../log"
LOG_FILE = "createworld.log"
DEFAULT_TERRAIN_CONFIG_DIR = 'configs'
DEFAULT_TERRAIN_CONFIG_FILE = 'default.terrain.config'
DEFAULT_CIVILIZATION_CONFIG_DIR = 'configs'
DEFAULT_CIVILIZATION_CONFIG_FILE = 'default.civilization.config'
SAVED_WORLDS_DIR = '../worlds'
WORLD_VERSION = '0.0.1'

## Helper Functions

def parse_arguments(args):
    # Create an argument parser
    my_parser = argparse.ArgumentParser(prog='createworld',
                                        description='Create a new world')

    # Add the arguments
    my_parser.add_argument('-n',
                            '--name',
                            help='the name of the new world')
    my_parser.add_argument('-v',
                            '--verbose',
                            action='store_true',
                            help='enable verbose output')
    my_parser.add_argument('-l',
                            '--verbose_logging',
                            action='store_true',
                            help='enable verbose logging')
    my_parser.add_argument('-t',
                            '--terrain-config',
                            help='the location of the terrain config file')
    my_parser.add_argument('-c',
                            '--civilization-config',
                            help='the location of the civilization config file')
    my_parser.add_argument('-o',
                            '--offset',
                            type=int,
                            help='the new world\'s offset in days')
    my_parser.add_argument('-i',
                            '--inactive',
                            action='store_true',
                            help='whether the world should start updating')
    my_parser.add_argument('--speed',
                            type=int,
                            help='the new world\'s simulation speed')
    my_parser.add_argument('--seed',
                            help='the new world\'s terrain seed')

    # Execute the parse_args() method
    return my_parser.parse_args(args)

def prepare_log_file(dir_, filename):
    if not os.path.exists(dir_):
        os.mkdir(dir_)
    path = os.path.join(dir_, filename)
    if os.path.exists(path):
        os.remove(path)

def prepare_worlds_dir(dir_):
    if not os.path.exists(dir_):
        os.mkdir(dir_)

def prepare_logger(verbose, verbose_logging, log_path):
    # Attach handlers for console and file output
    console_handler = logging.StreamHandler(sys.stdout)

    if verbose:
        console_handler.setLevel(logging.DEBUG)
    else:
        console_handler.setLevel(logging.INFO)

    file_handler = logging.FileHandler(log_path)

    if verbose_logging:
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    logger.info('Using log file located at: ' + log_path)
    return logger

def get_empty_world(logger):
    logger.debug('Creating a new world')
    world = {}
    world['uuid'] = uuid.uuid4()
    logger.debug('Generating new UUID: ' + world['uuid'].hex + '.')
    return world

def get_world_name(logger, args, uuid_):
    world_name = ''
    if args.name != None:
        world_name = args.name
        logger.debug('Setting name to given name: ' + args.name + '.')
    else:
        world_name = uuid_.hex
        logger.debug('No name given; Setting name to uuid.')
    return world_name

def get_world_datetimes(logger):
    world_datetime = datetime.datetime.now(datetime.timezone.utc)
    logger.debug('Setting datetimes: ' + world_datetime.isoformat())
    return world_datetime

def get_world_version(logger):
    # Use a hardcoded version for now until versioning is implemented
    logger.debug('Setting version: ' + WORLD_VERSION)
    return WORLD_VERSION

def get_world_offset(logger, args, world_datetime):
    # Set the offset (defaults to now)
    world_offset = world_datetime
    if args.offset != None:
        logger.debug('Setting offset to given value: ' + str(args.offset) + '.')
        world_offset = world_datetime + datetime.timedelta(days = args.offset)
    else:
        logger.debug('No offset given; Setting offset to 0.')
    return world_offset

def get_world_active_state(logger, args):
    world_active = not args.inactive
    logger.debug('Setting active state: ' + str(world_active) + '.')
    return world_active

def get_world_speed(logger, args):
    world_speed = 1
    if args.speed != None:
        if args.speed < 0:
            logger.warning('World speed given was below 0. Setting world speed to 0.')
            world_speed = 0
        else:
            world_speed = args.speed
            logger.debug('Setting the world speed to given value: ' + str(world_speed))
    else:
        logger.debug('No world speed given; Setting world speed to 1.')
    return world_speed

def get_world_seed(logger, args):
    terrain_seed = ''
    if args.seed != None:
        terrain_seed = args.seed
        logger.debug('Setting the terrain seed to given value: ' + terrain_seed + '.')
    else:
        # Generate a pseudorandom seed
        terrain_seed = os.urandom(4).hex()
        logger.debug('No terrain seed given; Generating new terrain seed: ' + terrain_seed + '.')
    return terrain_seed

def write_world_to_file(logger, world, dir_):
    logger.debug('Writing out world to file in worlds folder.')
    world_path = os.path.join(dir_, world['name'])
    with open(world_path, 'wb') as output:
        pickle.dump(world, output, -1)
        logger.debug('World written to file: ' + world_path + '.')

def create_new_world():
    ## Stage 0 - Arguments
    # Parsing arguments goes here

    args = parse_arguments(sys.argv[1:])    # Remove the script name from the arguments

    ## Stage 1 - Setup and logging
    # Any environment setup and logging setup goes here

    prepare_log_file(LOG_DIR, LOG_FILE)

    prepare_worlds_dir(SAVED_WORLDS_DIR)

    logger = prepare_logger(args.verbose, args.verbose_logging, LOG_DIR + '/' + LOG_FILE)

    ## Stage 2 - Configuration
    # Parsing config files for additional configuration and creating the world object goes here

    # Initialize the persistent world as an empty dictionary
    world = get_empty_world(logger)

    world['name'] = get_world_name(logger, args, world['uuid'])

    world['created_datetime'] = world['updated_datetime'] = get_world_datetimes(logger)

    world['created_version'] = world['updated_version'] = get_world_version(logger)

    world['offset_datetime'] = get_world_offset(logger, args, world['created_datetime'])

    world['active'] = get_world_active_state(logger, args)

    world['speed'] = get_world_speed(logger, args)

    world['seed'] = get_world_seed(logger, args)

    # Set the terrain and civilization config options
    # We'll do this once there are config options

    logger.debug('Setting empty terrain and civilization configs.')
    world['terrain_config'] = ''
    world['civilization_config'] = ''

    ## Stage 3 - Terrain generation
    # A call to the terrain generator goes here


    ## Stage 4 - Populating world
    # Adding starting population goes here

    ## Stage 5 - Upload world
    # Uploading the new world to the database or saving it locally goes here

    write_world_to_file(logger, world, SAVED_WORLDS_DIR)

    ## Stage 6 - Cleanup
    # Environmental cleanup goes here

if __name__ == '__main__':
    create_new_world()