# -*- coding: utf-8 -*-

from context import createworldscript
from createworldscript import createworld
import unittest
from unittest.mock import patch, MagicMock
import logging
import os
import uuid
import datetime

class test_createworld(unittest.TestCase):

    def test_parse_arguments(self):
        arguments = ['-n=a', '-v', '-l', '-t=b', '-c=c', '-o=1', '-i', '--speed=2', '--seed=d']
        args = createworld.parse_arguments(arguments)
        self.assertEqual(args.name, 'a')
        self.assertEqual(args.verbose, True)
        self.assertEqual(args.verbose_logging, True)
        self.assertEqual(args.terrain_config, 'b')
        self.assertEqual(args.civilization_config, 'c')
        self.assertEqual(args.offset, 1)
        self.assertEqual(args.inactive, True)
        self.assertEqual(args.speed, 2)
        self.assertEqual(args.seed, 'd')

    def test_prepare_log_file(self):
        dir_ = './test_logs'
        filename = 'test_log'
        path = os.path.join(dir_, filename)

        # Set up cleanup to run even in case of failure
        def on_cleanup():
            if os.path.exists(path):
                os.remove(path)
            if os.path.exists(dir_):
                os.rmdir(dir_)
        self.addCleanup(on_cleanup)

        createworld.prepare_log_file(dir_, filename)
        self.assertTrue(os.path.exists(dir_))
        self.assertFalse(os.path.exists(path))
        os.mknod(path)
        createworld.prepare_log_file(dir_, filename)
        self.assertFalse(os.path.exists(path))

    def test_prepare_worlds_dir(self):
        dir_ = './test_worlds'

        # Set up cleanup to run even in case of failure
        def on_cleanup():
            if os.path.exists(dir_):
                os.rmdir(dir_)
        self.addCleanup(on_cleanup)

        createworld.prepare_worlds_dir(dir_)
        self.assertTrue(os.path.exists(dir_))

    def test_prepare_logger(self):
        path = './test_log'

        # Set up cleanup to run even in case of failure
        def on_cleanup():
            if os.path.exists(path):
                os.remove(path)
        self.addCleanup(on_cleanup)

        logger = createworld.prepare_logger(True, True, path)
        self.assertEqual(logger.handlers[0].level, logging.DEBUG)
        self.assertEqual(logger.handlers[1].level, logging.DEBUG)

    def test_get_empty_world(self):
        logger = logging.getLogger(__name__)
        world = createworld.get_empty_world(logger)
        self.assertEqual(world['uuid'].__class__, uuid.UUID)

    def test_get_world_name(self):
        logger = logging.getLogger(__name__)
        args = createworld.parse_arguments([])
        world = {}
        uuid_ = uuid.uuid4()
        name = createworld.get_world_name(logger, args, uuid_)
        self.assertEqual(name, uuid_.hex)
    
    def test_getworlddattimes(self):
        logger = logging.getLogger(__name__)
        datetime_ = createworld.get_world_datetimes(logger)
        self.assertEqual(datetime_.tzinfo, datetime.timezone.utc)
        
    def test_get_world_version(self):
        logger = logging.getLogger(__name__)
        version = createworld.get_world_version(logger)
        self.assertEqual(version, '0.0.1')

    def test_get_world_offset(self):
        logger = logging.getLogger(__name__)
        args = createworld.parse_arguments(['-o=-10'])
        datetime_ = datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc)
        expectedoffset = datetime.datetime(1999, 12, 22, tzinfo=datetime.timezone.utc)
        offset = createworld.get_world_offset(logger, args, datetime_)
        self.assertEqual(offset, expectedoffset)

    def test_get_world_active_state(self):
        logger = logging.getLogger(__name__)
        args = createworld.parse_arguments(['-i'])
        activestate = createworld.get_world_active_state(logger, args)
        self.assertEqual(activestate, False)

    def test_get_world_speed(self):
        logger = logging.getLogger(__name__)
        args = createworld.parse_arguments(['--speed=2'])
        speed = createworld.get_world_speed(logger, args)
        self.assertEqual(speed, 2)

    def test_get_world_seed(self):
        logger = logging.getLogger(__name__)
        args = createworld.parse_arguments(['--seed=a'])
        seed = createworld.get_world_seed(logger, args)
        self.assertEqual(seed, 'a')

    def test_write_world_to_file(self):
        logger = logging.getLogger(__name__)
        world = {}
        world['name'] = uuid.uuid4().hex
        dir_ = './test_worlds'
        path = os.path.join(dir_, world['name'])
        if not os.path.exists(dir_):
            os.mkdir(dir_)

        # Set up cleanup to run even in case of failure
        def on_cleanup():
            if os.path.exists(path):
                os.remove(path)
            if os.path.exists(dir_):
                os.rmdir(dir_)
        self.addCleanup(on_cleanup)

        createworld.write_world_to_file(logger, world, dir_)
        self.assertTrue(os.path.exists(path))

    @patch('createworldscript.createworld.prepare_log_file')
    @patch('createworldscript.createworld.prepare_worlds_dir')
    @patch('createworldscript.createworld.prepare_logger', MagicMock(return_value=logging.getLogger(__name__)))
    @patch('createworldscript.createworld.get_empty_world', MagicMock(return_value={'uuid':'a'}))
    @patch('createworldscript.createworld.get_world_name', MagicMock(return_value='b'))
    @patch('createworldscript.createworld.get_world_datetimes', MagicMock(return_value='c'))
    @patch('createworldscript.createworld.get_world_version', MagicMock(return_value='d'))
    @patch('createworldscript.createworld.get_world_offset', MagicMock(return_value='e'))
    @patch('createworldscript.createworld.get_world_active_state', MagicMock(return_value='f'))
    @patch('createworldscript.createworld.get_world_speed', MagicMock(return_value='g'))
    @patch('createworldscript.createworld.get_world_seed', MagicMock(return_value='h'))
    @patch('createworldscript.createworld.write_world_to_file')
    def test_create_new_world(self, mock_a, mock_b, mock_c):
        logger = logging.getLogger(__name__)
        # Get some empty arguments
        testarguments = createworld.parse_arguments([])

        # Mock everything we're testing elsewhere
        #createworld.parse_arguments = MagicMock(return_value=testarguments)
        #createworld.prepare_log_file = MagicMock()
        #createworld.prepare_worlds_dir = MagicMock()
        #createworld.prepare_logger = MagicMock(return_value=logger)
        #createworld.get_empty_world = MagicMock(return_value={'uuid':'a'})
        #createworld.get_world_name = MagicMock(return_value='b')
        #createworld.get_world_datetimes = MagicMock(return_value='c')
        #createworld.get_world_version = MagicMock(return_value='d')
        #createworld.get_world_offset = MagicMock(return_value='e')
        #createworld.get_world_active_state = MagicMock(return_value='f')
        #createworld.get_world_speed = MagicMock(return_value='g')
        #createworld.get_world_seed = MagicMock(return_value='h')
        #createworld.write_world_to_file = MagicMock()

        with patch('createworldscript.createworld.parse_arguments', MagicMock(return_value=testarguments)):
            createworld.create_new_world()
        
        # Make sure the resulting world is accurate
        expectedworld = {'uuid':'a', 'name':'b', 'created_datetime':'c', 'updated_datetime':'c',
                         'created_version':'d', 'updated_version':'d', 'offset_datetime':'e',
                         'active':'f', 'speed':'g', 'seed':'h', 'terrain_config':'',
                         'civilization_config':''}
        createworld.write_world_to_file.assert_called_with(logger, expectedworld, createworld.SAVED_WORLDS_DIR)

if __name__ == '__main__':
    unittest.main()