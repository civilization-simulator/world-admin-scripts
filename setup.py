# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='world-admin-scripts',
    version='0.0.1',
    description='These are the scripts used to create, update, and edit worlds for the civilization simulator.',
    long_description=readme,
    author='Kobi Shoults',
    author_email='kobishoults@gmail.com',
    url='https://gitlab.com/civilization-simulator/world-admin-scripts.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)