# World Admin Scripts

This simulation of a planet full of civilizations growing is meant primarily to be a showcase of my abilities and secondarily as a way to improve my skills with python and django. It will encompass a cylindrical map upon which many terrain features will be placed algorithmically. Populations will then be placed in this world to spread and develop. There will be a number of world start options that I will develop over time to provide customization to the terrain as well as the initial population distribution. Additionally, I’ll introduce additional rules to represent the development of populations such as technological and societal advancement. While all of the worlds will be viewable at all times, only some worlds are going to be simulated at any one time.

These are the scripts used to create, update, and edit worlds for the civilization simulator.
